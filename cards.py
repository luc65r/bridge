from typing import List
from enum import Enum
from random import shuffle

class CustomEnum(Enum):
    short: str

    def __new__(cls, v: int, n: str) -> 'CustomEnum':
        obj = object.__new__(cls)
        obj._value_ = v
        obj.short = n
        return obj

    def __str__(self) -> str:
        return self.short

    def __ge__(self, other: 'CustomEnum') -> bool:
        if self.__class__ is other.__class__:
            return self.value >= other.value
        return NotImplemented
    def __gt__(self, other: 'CustomEnum') -> bool:
        if self.__class__ is other.__class__:
            return self.value > other.value
        return NotImplemented
    def __le__(self, other: 'CustomEnum') -> bool:
        if self.__class__ is other.__class__:
            return self.value <= other.value
        return NotImplemented
    def __lt__(self, other: 'CustomEnum') -> bool:
        if self.__class__ is other.__class__:
            return self.value < other.value
        return NotImplemented


class Suit(CustomEnum):
    Club    = 1, '♣'
    Diamond = 2, '♦'
    Heart   = 3, '♥'
    Spade   = 4, '♠'

class Value(CustomEnum):
    Two   = 2 , '2'
    Three = 3 , '3'
    Four  = 4 , '4'
    Five  = 5 , '5'
    Six   = 6 , '6'
    Seven = 7 , '7'
    Eight = 8 , '8'
    Nine  = 9 , '9'
    Ten   = 10, '10'
    Jack  = 11, 'J'
    Queen = 12, 'Q'
    King  = 13, 'K'
    Ace   = 14, 'A'


class Card:
    """
    A class used to represent a card.

    Attributes:

    value: Value
        The value of the card.

    suit: Suit
        The suit of the card.
    """

    value: Value
    suit: Suit

    def __init__(self, value: Value, suit: Suit) -> None:
        self.value = value
        self.suit = suit

    def __repr__(self) -> str:
        return f"Card({self.value!r}, {self.suit!r})"

    def __str__(self) -> str:
        return f"{self.value}{self.suit}"


class Deck:
    """
    A class used to represent a deck.

    Attributes:

    cards: List[Card]
        The list of cards.
    """

    cards: List[Card]

    def __init__(self, *, full: bool = False,
                 shuffle: bool = False) -> None:
        """Create an empty deck.

        Options:

        full
            Fill the deck with 52 cards.

        shuffle
            Shuffle the deck.

        Note: you can't use shuffle without full!
        """
        if not full ** shuffle: # Logical implication
            raise Exception("It is useless to shuffle an empty deck!")

        if full:
            self.fill()
        else:
            self.cards = []

        if shuffle:
            self.shuffle()

    def fill(self) -> None:
        """Fill the deck with the 52 cards."""
        self.cards = [Card(v, s) for s in list(Suit) for v in list(Value)]

    def __repr__(self) -> str:
        return 'Deck(' + ', '.join(map(repr, self.cards)) + ')'

    def __str__(self) -> str:
        return ' '.join(map(str, self.cards))

    def shuffle(self) -> None:
        """Shuffle the deck."""
        shuffle(self.cards)

    def take(self, other: 'Deck', n: int = 1) -> None:
        """Take n cards from the other Deck"""
        assert n > 0, "Can't take less than 1 card!"
        for _ in range(n):
            self.cards.append(other.cards.pop())

    def give(self, other: 'Deck', n: int = 1) -> None:
        """Give n cards to the other Deck"""
        assert n > 0, "Can't give less than 1 card!"
        for _ in range(n):
            other.cards.append(self.cards.pop())


class Player:
    deck: Deck

    def __init__(self) -> None:
        self.deck = Deck()


class Team:
    pass


class Game:
    pass
